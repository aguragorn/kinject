# KInject [![](https://jitpack.io/v/com.gitlab.aguragorn/kinject.svg)](https://jitpack.io/#com.gitlab.aguragorn/kinject)
Dependency injection for your kotlin multiplatform project
## Installation
1. Add jitpack to repositories in your root build.gradle

    **Gradle DSL**
    ```
    allprojects {
        repositories {
            ...
            maven { url 'https://jitpack.io' }
        }
    }
    ```
   **Kotlin DSL**
   ```
   allprojects {
       repositories {
           ...
           maven { url = uri("https://jitpack.io") }
       }
   }
   ```
2. Add the dependency
    
    **Gradle DSL**
    ```
    dependencies {
        implementation 'com.gitlab.aguragorn.kinject:KInject:$VERSION'
    }
    ```
   **Kotlin DSL**
   ```
   dependencies {
        implementation("com.gitlab.aguragorn.kinject:KInject:$VERSION")
   }
   ```
## Usage
### Initialize DI
```
Provider.init()
```
### Register an Instance Provider
```
class ClassA : InterfaceA

Provider.register { ClassA() as InterfaceA }
```
### Register a Singleton Instance
```
class ClassB : InterfaceB

Provider.register { ClassB() as InterfaceB }
```
### Inject Instance
```
// inject using delegate
val objectB: InterfaceB by Injection()

// or get an instance from provider
val objectA = Provider.getInstance()
```
### Registering with Identifier
Sometimes you want to inject multiple concrete classes that implement
the same super class or interface. You can use an identifier to
determine which provider/instance to inject.
```
class ClassC : InterfaceC
class ClassD : InterfaceC
...
Provider.register(id = "class-c") { ClassC() as InterfaceC }
Provider.register(id = "class-d") { ClassD() as InterfaceD }
...
val objectC: InterfaceC by Injection("class-c")
val objectD: InterfaceD = Provider.getInstance(class-d")
```