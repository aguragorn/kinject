package com.aguragorn.kinject

import co.touchlab.stately.collections.sharedMutableMapOf
import kotlin.reflect.KProperty

/**
 * Keeps track of registered implementations for all Types
 */
open class Provider<T>
@PublishedApi
internal constructor(val create: () -> T) {
    fun instance(): T = create()

    companion object Factory {
        @PublishedApi
        internal var providerMap by atomic(sharedMutableMapOf<InstanceIdentifier, Provider<*>>())
            private set

        @PublishedApi
        internal var singletonMap by atomic(sharedMutableMapOf<InstanceIdentifier, Any>())
            private set

        fun init() {
            providerMap.dispose()
            singletonMap.dispose()
            providerMap = sharedMutableMapOf()
            singletonMap = sharedMutableMapOf()
        }

        /**
         * Registers an instance creator as a provider. A unique [id] can be supplied for multiple
         * creators for the same type. If multiple creators with the same id (or if no ids were supplied)
         * are registered, the last creator will always be used.
         *
         * ### Usage
         *     interface A
         *     class B : A
         *     class C : A
         *     Provider.register { arg -> B() as A }
         *     Provider.register(id = "uniqueID") { arg -> C() as A }
         * @param id unique identifier for the creator being registered. Can be null.
         * @param instanceCreator a lambda expression that will create an instance of the requested type [T].
         * @param T the type associated to the creator being registered.
         * @since 0.0.1
         */
        inline fun <reified T : Any> register(
            id: Any? = null,
            singleton: Boolean = false,
            noinline instanceCreator: () -> T
        ) {
            val identifier = InstanceIdentifier(T::class, id)

            if (singleton) {
                // use a provider that gets an instance from the singleton instances
                providerMap[identifier] = SingletonProvider(instanceCreator)
            } else {
                // just use the provided instance creator
                providerMap[identifier] = Provider(instanceCreator)
            }
        }

        inline fun <reified T : Any> getInstance(id: Any? = null): T {
            val identifier = InstanceIdentifier(T::class, id)

            val provider =
                providerMap[identifier] ?: throw NoProviderFoundException("No provider found for ${T::class}")

            val instance = try {
                if (provider is SingletonProvider) {
                    // if the provider is singleton it must be the first time
                    // an instance has been requested, we must then create
                    // a singleton instance which will be returned on
                    // succeeding calls to getInstance
                    return (provider.instance() as T).also { newInstance ->
                        singletonMap[identifier] = newInstance
                        providerMap[identifier] = Provider { singletonMap[identifier] }
                    }
                } else {
                    provider.instance()
                }
            } catch (e: Throwable) {
                throw InstanceCreationFailedException("instance creation failed for ${T::class}", e)
            }

            return instance as? T ?: throw InstanceCreationFailedException("provider returned null for ${T::class}")
        }

        inline fun <reified T : Any> getInstances(): List<T> {
            val keys = providerMap.keys.filter { it.type == T::class }
            val instances = mutableListOf<T>()

            for (key in keys) {
                val provider = providerMap[key] ?: continue

                // if the provider is singleton it must be the first time
                // an instance has been requested, we must then create
                // a singleton instance which will be returned on
                // succeeding calls to getInstance
                if (provider is SingletonProvider) {
                    singletonMap[key] = provider.instance() as T
                    providerMap[key] = Provider { singletonMap[key] }
                }

                instances.add(providerMap[key]?.instance() as? T ?: continue)
            }

            return instances
        }
    }

}

@PublishedApi
internal class SingletonProvider<T>(create: () -> T) : Provider<T>(create)

/**
 * A delegate for injecting instances to properties
 */
class Injection(val id: Any? = null) {
    @PublishedApi
    internal var value: Any? = null

    inline operator fun <reified T : Any> getValue(thisRef: Any?, property: KProperty<*>): T {
        return (value as? T) ?: Provider.getInstance<T>(id).also { value = it }
    }
}

@PublishedApi
internal data class InstanceIdentifier(val type: Any, val id: Any?)

class NoProviderFoundException(msg: String) : Exception(msg)
class InstanceCreationFailedException(msg: String, cause: Throwable? = null) : Exception(msg, cause)