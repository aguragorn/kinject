package com.aguragorn.kinject

import co.touchlab.stately.concurrency.AtomicBoolean
import co.touchlab.stately.concurrency.AtomicInt
import co.touchlab.stately.concurrency.AtomicLong
import co.touchlab.stately.concurrency.AtomicReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

internal class AtomicReferenceDelegate<T>(initial: T)
    : ReadWriteProperty<Any?, T> {

    private val atomic = AtomicReference(initial)

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        atomic.set(value)
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>) = atomic.get()
}

internal class AtomicIntDelegate(initial: Int)
    : ReadWriteProperty<Any?, Int> {

    private val atomic = AtomicInt(initial)

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        atomic.set(value)
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>) = atomic.get()
}

internal class AtomicBooleanDelegate(initial: Boolean)
    : ReadWriteProperty<Any?, Boolean> {

    private val atomic = AtomicBoolean(initial)

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
        atomic.value = value
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>) = atomic.value
}

internal class AtomicLongDelegate(initial: Long)
    : ReadWriteProperty<Any?, Long> {

    private val atomic = AtomicLong(initial)

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
        atomic.set(value)
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>) = atomic.get()
}


internal fun <T> atomic(initial: T) = AtomicReferenceDelegate(initial)

internal fun atomic(initial: Int) = AtomicIntDelegate(initial)

internal fun atomic(initial: Long) = AtomicLongDelegate(initial)

internal fun atomic(initial: Boolean) = AtomicBooleanDelegate(initial)