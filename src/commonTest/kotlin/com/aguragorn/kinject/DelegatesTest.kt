package com.aguragorn.kinject

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

private class TestObject
private object SharedTestObject {
    var intValue by atomic(0)
    var longValue by atomic(0L)
    var booleanValue by atomic(true)
    var testObjectValue by atomic(TestObject())
}

class DelegatesTest {
    @Test
    fun changing_int_value_does_not_crash() {
        try {
            SharedTestObject.intValue = 5
            assertEquals(5, SharedTestObject.intValue)
        } catch (e: Throwable) {
            fail(cause = e)
        }
    }

    @Test
    fun changing_long_value_does_not_crash() {
        try {
            SharedTestObject.longValue = 5L
            assertEquals(5L, SharedTestObject.longValue)
        } catch (e: Throwable) {
            fail(cause = e)
        }
    }

    @Test
    fun changing_boolean_value_does_not_crash() {
        try {
            SharedTestObject.booleanValue = false
            assertEquals(false, SharedTestObject.booleanValue)
        } catch (e: Throwable) {
            fail(cause = e)
        }
    }

    @Test
    fun changing_reference_value_does_not_crash() {
        try {
            val testObject = TestObject()
            SharedTestObject.testObjectValue = testObject
            assertEquals(testObject, SharedTestObject.testObjectValue)
        } catch (e: Throwable) {
            fail(cause = e)
        }
    }
}