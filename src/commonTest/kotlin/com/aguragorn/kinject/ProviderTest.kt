package com.aguragorn.kinject

import kotlin.test.*

class TestClass

class ProviderTest {
    @BeforeTest
    fun setUp() {
        Provider.init()
    }

    @Test
    fun provider_returns_same_instance_of_registered_singletons() {
        Provider.register(singleton = true) { TestClass() }

        val first: TestClass by Injection()
        val second: TestClass = Provider.getInstance()

        assertEquals(first, second)
    }

    @Test
    fun provider_returns_new_instance_of_registered_non_singletons() {
        Provider.register { TestClass() }

        val first: TestClass by Injection()
        val second: TestClass = Provider.getInstance()

        assertNotEquals(first, second)
    }

    @Test
    fun provider_returns_all_instances_resolved_for_type() {
        Provider.register(id = "first", singleton = true) { TestClass() }
        Provider.register(id = "second", singleton = true) { TestClass() }

        val instances = Provider.getInstances<TestClass>()
        val instances2 = Provider.getInstances<TestClass>()

        assertEquals(2, instances.size)
        assertEquals(2, instances2.size)
        assertTrue(instances.containsAll(instances2))
        assertTrue(instances2.containsAll(instances))
    }
}